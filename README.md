# Evergreen OSX Widget

## Install
- Télécharger [Today-scripts](https://github.com/lsd/Today-Scripts/raw/master/build/Today%20Scripts.app.zip). C'est une app qui permet d'afficher le résultat de scripts dans la section Today d'OSX.
- Cloner ce repo, ou copier le script `index.js` localement.
- `chmod +x /path/to/index.js` pour le rendre executable.
- Ajouter le widget `TodayScripts` dans Today.
- Ajouter un script:
    - le nom de votre choix
    - laissez le shell par défaut
    - entrez le chemin du script `path/to/index.js`.
 
 Et voilà !
 
 ![widget](./evergreen-osx-widget.png)