#!/usr/local/bin/node

const fetch = require('node-fetch');
const chalk = require('chalk');
const { parseISO, format } = require('date-fns');
const { fr } = require('date-fns/locale');
const log = console.log;

const EG_BASE_URL = 'https://kobusapp.com/evergreen/v1';
const EG_STAGE_LIST = `${EG_BASE_URL}/stage/list/json`;
const EG_STAGE_DETAILS = stage => `${EG_BASE_URL}/release/${stage}/json`

const ARTIFACTS = ['Win32', 'macOS', 'macOSAlt', 'bundle'];

function getPayload(url) {
    return fetch(url)
        .then(res => res.json())
        .then(res => res.payload);
}

function getFormattedDate(str) {
    if (!str) {
        return;
    }
    return format(parseISO(str), 'Pp', { locale: fr });
}

async function showEvergreenStageDetails(name) {
    const stage = await getPayload(EG_STAGE_DETAILS(name));

    log(`${chalk.yellow(stage.branch)} - ${chalk.whiteBright(stage.appVersion)}`);

    ARTIFACTS.forEach(art => {
        const { appHash, timestamp, runnerHash } = stage[art];

        log(
            `  ${chalk.white(art)}\n`,
            `    appHash: ${chalk.blue(appHash)}\n`,
            `    date: ${chalk.blue(getFormattedDate(timestamp))}\n`,
            runnerHash ? `    runnerHash: ${chalk.blue(runnerHash)}\n` : '',
        )
    })

}

async function showEvergreenStages() {
    const list = await getPayload(EG_STAGE_LIST);

    const STAGES = [];

    list.forEach(stage => {
        const releaseDate = getFormattedDate(stage.timestamp);
        const releaseDateStr = releaseDate ? chalk.blue(`- ${releaseDate}`) : '';

        STAGES.push(stage.releaseName);
        log(`${chalk.green(stage.name)}: ${chalk.yellow(stage.releaseName)} ${releaseDateStr}`);
    });

    log(chalk.white(`\n-------------\n`));

    STAGES.forEach(showEvergreenStageDetails);
}

showEvergreenStages();